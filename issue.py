import datetime

class Issue:
    """
    Holds issue data for writing csv.
    """
    FIELDS = ['issue', 'severity', 'created', 'triaged', 'updated',
              'closed', 'author', 'culprit', 'assignee', 'labels',
              'flags', 'state', 'fixes', 'title', 'gitlab_state']
    GITLAB_FIELDS = { 'iid': 'issue',
                      'created_at': 'created',
                      'closed_at': 'closed',
                      'updated_at': 'updated',
                      'title': 'title',
                      'state': 'gitlab_state',
                     }

    def __init__(self, from_csv=None, from_gitlab=None):
        self.issue = None
        self.severity = None
        self.created = None
        self.triaged = None
        self.updated = None
        self.closed = None
        self.author = None
        self.culprit = None
        self.assignee = None
        self.labels = None
        self.flags = None
        self.state = None
        self.fixes = None
        self.title = None
        self.gitlab_state = None

        if from_csv:
            for field in Issue.FIELDS:
                self.__dict__[field] = from_csv[field]
            return
        if from_gitlab:
            self.author = from_gitlab.author['username']
            self.assignee = None
            if from_gitlab.assignee:
                self.assignee = from_gitlab.assignee['username']
            from_gitlab.labels.sort()
            self.labels = ", ".join(from_gitlab.labels)
            for gitlab_field, issue_field in Issue.GITLAB_FIELDS.items():
                gitlab_value = None
                if gitlab_field in from_gitlab._attrs:
                    gitlab_value = from_gitlab._attrs[gitlab_field]
                self.__dict__[issue_field] = gitlab_value
            self.state = 'NeedsTriage'
            self.check_dont_care()
            if self.closed:
                self.state = 'Closed'

    def check_dont_care(self):
        if self.state != "NeedsTriage":
            return
        for dont_care in ["ACO", "AMD common", "CI", "CI alert", "CI daily",
                          "GLVND", "ISL", "Not Our Bug",
                          "OpenCL", "Project Access Requests", "RADV", "TC", "To Do",
                          "VA-API", "VDPAU", "Windows", "asahi", "bifrost", "big endian",
                          "clover", "coverity", "crocus", "d3d12", "difficulty: easy",
                          "difficulty: hard", "difficulty: medium", "docs",
                          "dozen", "drisw", "enhancement", "etnaviv", "feature",
                          "freedreno", "good-first-task", "haiku", "i915g", "incident",
                          "ir3", "isaspec", "kopper", "labels", "lavapipe", "lima",
                          "llvmpipe", "loader", "macOS", "maintainer-scripts", "mapi",
                          "mediump", "midgard", "nine", "nouveau", "nv-vieux", "nv30",
                          "nv50", "nvc0", "osmesa", "panfrost", "panvk", "perfetto",
                          "question", "r100", "r200", "r300", "r600", "radeonsi",
                          "softpipe", "svga", "swr", "swrast", "tegra", "tracker",
                          "turnip", "v3d", "v3dv", "vbo", "vc4", "venus", "virgl",
                          "vmwgfx", "xfail", "zink"]:
            if dont_care in self.labels:
                self.state = "NotOurBug"
                self.flags = "NotOurBug"
                self.severity = "6"
                self.triaged = datetime.date.today().strftime("%Y-%m-%d")

    def update(self, _gitlab_issue):
        """update fields in the issue based on changes to the gitlab issue"""
        new_revision = Issue(from_gitlab=_gitlab_issue)
        for field in Issue.FIELDS:
            if field in ['severity', 'triaged', 'culprit', 'state']:
                continue
            self.__dict__[field] = new_revision.__dict__[field]
        self.check_dont_care()
        if self.closed:
            self.state = 'Closed'
