#!/usr/bin/python

import csv
import datetime
import os
import sys

import gitlab
import update_severity
from issue import Issue
import utils

def intel_dev_issues_found():
    gl = gitlab.Gitlab('https://gitlab.freedesktop.org')
    mesa = gl.projects.get(176)
    known_issues = {}
    start_of_quarter = utils.first_day_of_this_quarter()
    parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
    issues_per_dev = {}

    for d in utils.intel_devs():
        issues_per_dev[d] = {'issue_count': 0, 'issue_numbers': []}

    # update severity csv first
    update_severity.update_severity()

    # parse known issues
    with open(f'{parent_dir}/severity.csv', encoding='utf-8') as fh:
        reader = csv.DictReader(fh, delimiter='\t')
        for row in reader :
            issue = Issue(from_csv=row)
            created = datetime.datetime.fromisoformat(issue.created.replace('Z', '+00:00'))

            if created.date() >= start_of_quarter:
                known_issues[int(issue.issue)] = issue

    with open(f'{parent_dir}/intel_dev_issues_found.csv', "w", encoding='utf-8') as fh:
        writer = csv.DictWriter(fh, delimiter='\t',
                                fieldnames=['developer', 'issues found this quarter', 'issue numbers'])
        writer.writeheader()

        for v in known_issues.values():
            try:
                issues_per_dev[v.author]['issue_count'] += 1
                issues_per_dev[v.author]['issue_numbers'].append(v.issue)
            except KeyError:
                continue

        for k,v in sorted(issues_per_dev.items(), key=lambda i: i[0]):
            writer.writerow({'developer': k, 'issues found this quarter': v['issue_count'], 'issue numbers': ' '.join(v['issue_numbers'])})

if __name__ == '__main__':
    intel_dev_issues_found()
