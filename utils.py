#!/usr/bin/python

import datetime

def first_day_of_this_quarter():
    today = datetime.date.today()
    return datetime.date(today.year, 3 * ((today.month - 1) // 3) + 1, 1)

def intel_devs():
    return ['idr',
            'kwg',
            'llandwerlin',
            'majanes',
            'nchery',
            'ngcortes',
            'tpalli',
            'shadeslayer',
            'zehortigoza',
            'ybogdano',
            'currojerez',
            'mslusarz',
            'jljusten',
            'pzanoni',
            'cmarcelo',
            'fjdegroo',
            'ibriano'
    ]
