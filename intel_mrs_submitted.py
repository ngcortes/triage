#!/usr/bin/python

import csv
import datetime
import os
import sys

import gitlab
import update_severity
from issue import Issue
import utils

def intel_mrs_submitted():
    gl = gitlab.Gitlab('https://gitlab.freedesktop.org')
    mesa = gl.projects.get(176)
    known_issues = {}
    start_of_quarter = utils.first_day_of_this_quarter()
    parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
    mrs_per_dev = {}

    for d in utils.intel_devs():
        mrs_per_dev[d] = {'mr_count': 0, 'mr_numbers': []}

    with open(f'{parent_dir}/intel_mrs_submitted.csv', "w", encoding='utf-8') as fh:
        mrs = mesa.mergerequests.list(all=True, created_after=start_of_quarter)
        writer = csv.DictWriter(fh, delimiter='\t',
                                fieldnames=['developer', 'mrs submitted this quarter', 'mr numbers'])
        writer.writeheader()

        for mr in mrs:
            try:
                mrs_per_dev[mr.author['username']]['mr_count'] += 1
                mrs_per_dev[mr.author['username']]['mr_numbers'].append(str(mr.iid))
            except KeyError:
                continue

        for k,v in sorted(mrs_per_dev.items(), key=lambda i: i[0]):
            writer.writerow({'developer': k, 'mrs submitted this quarter': v['mr_count'], 'mr numbers': ' '.join(v['mr_numbers'])})

if __name__ == '__main__':
    intel_mrs_submitted()
